words
=====

My SO was playing a game where you are given variable number of letters and you have to find words you can make out of these given letters. Letters must only be used once and the length of the words to complete the level are given. So I wrote this little script that can be used to automatically find these words.

It first calculates all combinations of chars and then uses hunspell, the spell checker of LibreOffice, to check which of these combinations are actually valid words. By default is used the German version of hunspell but any other hunspell version could be used by changing `dict = enchant.Dict("your_language")`.

## Requirements
* Python 3
* PyEnchant
    * `pip install pyenchant`
    * `pacman -S python-pyenchant`

## Usage

```bash
$ python words.py
Enter the number of chars: 6
----------------------------
1: e
2: u
3: r
4: o
5: p
6: a
----------------------------
Combinations count: 3900
4.876134157180786 seconds
Word length? '0' to exit. 6
Europa
----------------------------
Word length? '0' to exit. 5
Raupe
----------------------------
Word length? '0' to exit. 4
Euro
Oper
Peru
Pore
pure
raue
----------------------------
Word length? '0' to exit. 0
```
