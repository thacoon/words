#!/usr/bin/python3

# pep8 but tabs allowed and line length of 120
# pycodestyle words.py --ignore=W191 --max-line-length=120

import enchant
import itertools
import time

start_time = time.time()

count = int(input("Enter the number of chars: "))
print("----------------------------")

chars = []
for i in range(1, count + 1):
	c = input("{}: ".format(i))
	chars.append(c)

combinations = []
# every char can be used once but word length ranges from 2 chars to number of chars given
# words have at least 2 chars
for r in range(2, count + 1):
	for w in itertools.permutations(chars, r=r):
		combinations.append("".join(w))

# capitalize every word, so nouns are recognised (German language)
for i in range(len(combinations)):
	combinations.append(combinations[i].capitalize())

print("----------------------------")
print("Combinations count:", len(combinations))

# use German dictionary
dict = enchant.Dict("de_DE")

found = []
for combination in combinations:
	is_correct = dict.check(combination)

	if is_correct:
		found.append(combination)

found_no_copies = found
for word in found:
	count = 0

	# remove duplicates from list
	# otherwise some duplicates if they have correct spelling if capitalized and if all lowercase
	for w in found_no_copies:
		if w.lower() == word.lower():
			count += 1

			if count > 1:
				found_no_copies.remove(w)

# sort list alphabetically
found_no_copies.sort()

print("%s seconds" % (time.time() - start_time))

while True:
	command = int(input("Word length? '0' to exit. "))

	if command == 0:
		exit()

	for w in found_no_copies:
		if len(w) == command:
			print(w)

	print("----------------------------")
